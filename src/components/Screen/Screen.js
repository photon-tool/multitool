import React from "react";
import { AppBar, CssBaseline } from "@material-ui/core";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Slide from "@material-ui/core/Slide";

function ElevationScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0
  });
}

function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

function Screen({ toolbar, toolbarMode = "elevate", children }) {
  return (
    <>
      <CssBaseline />
      {toolbarMode === "elevate" && (
        <ElevationScroll>
          <AppBar>{toolbar}</AppBar>
        </ElevationScroll>
      )}
      {toolbarMode === "scroll" && (
        <HideOnScroll>
          <AppBar>{toolbar}</AppBar>
        </HideOnScroll>
      )}
      {children}
    </>
  );
}

export default Screen;
