import React from "react";
import Routes from "./Routes";
import { BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider } from "@material-ui/styles";
import { createMuiTheme } from "@material-ui/core";
import { blue } from "@material-ui/core/colors";

function App() {
  return (
    <ThemeProvider theme={createMuiTheme({ palette: { primary: blue } })}>
      <Router>
        <Routes />
      </Router>
    </ThemeProvider>
  );
}

export default App;
