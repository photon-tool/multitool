import React from "react";
import { Route, Switch } from "react-router-dom";
import MainScreen from "../../screens/MainScreen";

function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={MainScreen} />
    </Switch>
  );
}

export default Routes;
