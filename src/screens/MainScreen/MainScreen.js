import React, { useState } from "react";
import {
  Toolbar,
  Box,
  Typography,
  List,
  ListSubheader,
  ListItem,
  ListItemText,
  Snackbar,
  Card
} from "@material-ui/core";
import Screen from "../../components/Screen";
import CommandInput from "./CommandInput";
import useRecentCommands from "../../hooks/useRecentCommands";
import useStatusBarContentType from "../../hooks/useStatusBarContentType";
import { PhotonTool, StatusBarContentType } from "@photon-os/sdk";
import moment from "moment";
import classes from "./MainScreen.module.scss";

function MainScreen() {
  useStatusBarContentType(StatusBarContentType.DarkContent);

  const [toastOpen, setToastOpen] = useState(false);
  const [toastMessage, setToastMessage] = useState("");
  const [command, setCommand] = useState("");
  const { commands, addCommand } = useRecentCommands();

  function runCommand(e) {
    e.preventDefault();
    addCommand(command);
    runRecentCommand(command);
    setCommand("");
  }

  async function runRecentCommand(recentCommand) {
    try {
      const tool = await PhotonTool.getPhotonTool();
      tool.invokeCommand(recentCommand);
      setToastMessage(`Ran "${recentCommand}".`);
      setToastOpen(true);
    } catch (err) {
      setToastMessage(err.message);
      setToastOpen(true);
    }
  }

  return (
    <Screen
      toolbar={
        <Toolbar>
          <Typography variant="h6" style={{ flexGrow: 1 }}>
            Multitool
          </Typography>
        </Toolbar>
      }
    >
      <Box className={classes.commandBox} marginTop="70px" p="1em">
        <CommandInput
          placeholder="Type a command..."
          value={command}
          onChange={e => setCommand(e.target.value)}
          onSubmit={runCommand}
        />
      </Box>

      <Box marginBottom="70px">
        <List>
          <ListSubheader>Recent Commands</ListSubheader>

          {commands.map((c, index) => (
            <ListItem
              key={index}
              onClick={() => runRecentCommand(c.command)}
              button
            >
              <ListItemText
                primary={c.command}
                secondary={moment
                  .unix(c.time / 1000)
                  .format("MMM. D [at] h:mm A")}
              />
            </ListItem>
          ))}
        </List>

        {commands.length === 0 && (
          <Box mx="1em">
            <Card elevation={0}>
              <Box padding="1em" mx="1em">
                <Typography align="center" variant="body2">
                  Commands you run will show up here.
                </Typography>
              </Box>
            </Card>
          </Box>
        )}
      </Box>

      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        open={toastOpen}
        onClose={() => setToastOpen(false)}
        message={toastMessage}
        autoHideDuration={2000}
        style={{ marginBottom: 70 }}
      />
    </Screen>
  );
}

export default MainScreen;
