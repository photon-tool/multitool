import React from "react";
import SearchIcon from "@material-ui/icons/ChevronRightRounded";
import { Paper, InputBase, CircularProgress } from "@material-ui/core";
import classNames from "classnames";
import classes from "./CommandInput.module.scss";

export default function CommandInput({
  placeholder,
  value,
  onChange,
  onSubmit,
  loading
}) {
  return (
    <Paper
      component="form"
      className={classes.commandInput}
      elevation={1}
      onSubmit={onSubmit}
    >
      <SearchIcon />
      <InputBase
        className={classNames(classes.inputBase, {
          [classes.loading]: loading
        })}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
      />
      {loading && <CircularProgress size={20} thickness={5.4} />}
    </Paper>
  );
}
