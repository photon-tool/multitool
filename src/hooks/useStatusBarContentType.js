import { LifecycleEvents, StatusBar } from "@photon-os/sdk";
import { useEffect } from "react";

function useStatusBarContentType(contentType, setOnForeground = false) {
  useEffect(
    () => {
      if (setOnForeground) {
        LifecycleEvents.on("willEnterForeground", () => {
          StatusBar.setContentType(contentType);
        });
      }

      StatusBar.setContentType(contentType);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );
}

export default useStatusBarContentType;
