import { useState, useEffect } from "react";

export default function useRecentCommands() {
  const [commands, setCommands] = useState([]);

  function addCommand(command) {
    const newCommands = [{ command, time: Date.now() }, ...commands];
    setCommands(newCommands);
    window.localStorage.setItem("recentCommands", JSON.stringify(newCommands));
  }

  useEffect(() => {
    const storedCommands = window.localStorage.getItem("recentCommands");
    if (!storedCommands) return;
    setCommands(JSON.parse(storedCommands));
  }, []);

  return { commands, addCommand };
}
